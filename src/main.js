import Vue from "vue";
import { Vuelidate } from "vuelidate";
import VueTheMask from "vue-the-mask";
import App from "./App.vue";
import VueTelInput from "vue-tel-input";
import vueCustomElement from "vue-custom-element";
import router from "./router";
import store from "./store";
import "document-register-element/build/document-register-element";
import "vue-tel-input/dist/vue-tel-input.css";
import "vuetify/dist/vuetify.min.css";

import axios from "axios";
import VueAxios from "vue-axios";
Vue.use(VueAxios, axios);

Vue.config.productionTip = false;

Vue.use(vueCustomElement);

Vue.use(Vuelidate);

Vue.use(VueTheMask);

Vue.router = router;
Vue.store = store;
//get.geojs.io/v1/ip/country.json
https: Vue.use(VueTelInput, {
  autoFormat: false,
  // defaultCountry: "ua",
  // autoDefaultCountry: true,

  inputOptions: {
    placeholder: "",
    showDialCode: true,
  },
  dropdownOptions: {
    width: "311px",
  },
});

Vue.customElement("vue-widget", App);
